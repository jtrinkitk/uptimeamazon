﻿$(document).ready(function () {

    var currencies = '';
    var currencyModifier = 1;

    var findCurrency = function (currencyName) {
        for (var i = 0, len = currencies.length; i < len; i++) {
            if (currencies[i].Name === currencyName)
                return i;
        }
        return null;
    }

    $(function () {
        $.ajax({
            url: "/Search/FetchCurrencies",
            datatype: "json",
            type: "POST",
            success: function (data) {
                currencies = data;
            },
            error: function () {
            }
        })
    });

    $('#searchResults').DataTable({
        "columns": [
            { title: "Name", "class": "name" },
            { title: "Price", "class": "price", "width": "60px" }
        ],
        "lengthChange": false,
        "pageLength": 13,
        "language": {
            "emptyTable": "No results found",
            "info": "Showing _START_ to _END_ of _TOTAL_ results",
            "infoEmpty": "Showing 0 to 0 of 0 results",
        },
        "conditionalPaging": true,
        "stateSave": true,
        "bFilter": false,
        "bInfo": false,
        "autoWidth": false,
    });

    $("#changeCurrency").change(function () {
        var table = $('#searchResults').DataTable();
        var oldModifier = currencyModifier;
        var selectedCurrencyIndex = findCurrency(document.getElementById('changeCurrency').value);
        currencyModifier = parseFloat(currencies[selectedCurrencyIndex].Rate.replace(',', '.'));
        table.cells('.price').every(function () {
            this.data(parseFloat(this.data() / oldModifier)); // back to USD as OpenExchangeRates gives rates based on USD
            this.data((parseFloat(this.data()) * currencyModifier).toFixed(2));
        });
        table.cells().invalidate().draw();
    });

    $("#searchButton").click(function () {
        $.ajax({
            url: "/Search/FetchItems",
            datatype: "json",
            type: "POST",
            data: { keyword: $("#searchBox").val() },
            success: function (data) {
                var table = $('#searchResults').DataTable();
                table.clear();
                table.draw();
                $.each(data, function (i, item) {
                    var listedPrice = item.Price * currencyModifier;
                    table.row.add(
                        ["<a href=" + item.Url + ">" + item.Name + "</a>", listedPrice.toFixed(2)]
                    ).draw();
                });
            },
            error: function () {
                table.clear()
                table.draw();
            }
        });
    });
});