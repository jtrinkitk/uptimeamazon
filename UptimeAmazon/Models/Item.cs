﻿using System;

namespace UptimeAmazon.Models
{
    public class Item
    {

        private string _name;
        private decimal _price;
        private string _url;

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public decimal Price
        {
            get
            {
                return _price;
            }

            set
            {
                _price = value;
            }
        }

        public string Url
        {
            get
            {
                return _url;
            }

            set
            {
                _url = value;
            }
        }



        public Item(string name, string providedPrice, string url)
        {
            this._name = name;
            this._price = Decimal.Divide(Decimal.Parse(providedPrice), 100M);
            this._url = url;
        }

    }
}