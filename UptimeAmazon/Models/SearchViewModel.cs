﻿namespace UptimeAmazon.Models
{
    public class SearchViewModel
    {
        public Item Item { get; set; }
        public Currency Currency { get; set; }
    }
}