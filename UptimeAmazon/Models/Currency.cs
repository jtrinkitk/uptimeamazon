﻿namespace UptimeAmazon.Models
{
    public class Currency
    {
        private string _name;
        private string _rate;
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public string Rate
        {
            get
            {
                return _rate;
            }

            set
            {
                _rate = value;
            }
        }

        #region constructors
        public Currency(string name, string rate)
        {
            this._name = name;
            this._rate = rate;
        }

        public Currency() { }
        #endregion
    }
}