﻿using AmazonProductAdvtApi;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Xml.Linq;
using UptimeAmazon.Models;

namespace UptimeAmazon.Controllers
{
    public class SearchController : Controller
    {
        private readonly string AWS_ACCESS_KEY = System.Configuration.ConfigurationManager.AppSettings["AwsAccessKey"];
        private readonly string AWS_SECRET_KEY = System.Configuration.ConfigurationManager.AppSettings["AwsSecretKey"];
        private readonly string ASSOCIATE_ID = System.Configuration.ConfigurationManager.AppSettings["AwsAssociateId"];
        private readonly string DESTINATION = System.Configuration.ConfigurationManager.AppSettings["AwsEndpoint"];
        private readonly string OEX_APP_ID = System.Configuration.ConfigurationManager.AppSettings["OexAppId"];

        public ActionResult Search()
        {
            return View();
        }

        [HttpPost]
        public JsonResult FetchItems(string keyword)
        {
            SignedRequestHelper helper = new SignedRequestHelper(AWS_ACCESS_KEY, AWS_SECRET_KEY, DESTINATION);

            string requestString = "Service=AWSECommerceService"
                    + "&Version=2009-03-31"
                    + "&Operation=ItemSearch"
                    + "&AssociateTag=juhaesim-20"
                    + "&SearchIndex=Blended" 
                    + "&ResponseGroup=ItemAttributes"
                    + "&Timestamp=" + DateTime.Now
                    + "&Keywords=" + keyword;

            var requestUrl = helper.Sign(requestString);
            //Debug.WriteLine(requestUrl); // View xml by hand when debugging

            var responseXml = XDocument.Load(requestUrl);

            XNamespace ns = "http://webservices.amazon.com/AWSECommerceService/2011-08-01";

            List<Item> items = responseXml.Descendants(ns + "Item")
                .Where(x => x.Element(ns + "ItemAttributes") != null) // Skipped check for IsValid == True because of these checks for null
                .Where(x => x.Element(ns + "ItemAttributes").Element(ns + "ListPrice") != null && x.Element(ns + "DetailPageURL") != null)
                .ToList()
                .Select(x => new Item
            (
                x.Element(ns + "ItemAttributes").Element(ns + "Title").Value,
                x.Element(ns + "ItemAttributes").Element(ns + "ListPrice").Element(ns + "Amount").Value,
                x.Element(ns + "DetailPageURL").Value)
            )
            .ToList();

            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FetchCurrencies()
        {
            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString("https://openexchangerates.org/api/latest.json?app_id=" + OEX_APP_ID);
                JObject jObj = JObject.Parse(json);
                List<Currency> currencies = jObj.SelectToken("rates").OfType<JProperty>().Select(y => new Currency
                {
                    Name = y.Name,
                    Rate = y.Value.ToString()
                })
                .ToList();
                return Json(currencies, JsonRequestBehavior.AllowGet);
            }
        }
    }
}