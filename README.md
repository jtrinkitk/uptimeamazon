# README #

Test project for Uptime Systems OÜ.

### Summary ###

* Web application that uses Amazon Product Advertising API to find products by keyword, using REST requests
* Request signatures are generated using Amazon's Signed Requests Sample Code
* Uses DataTables (https://datatables.net/) to display the table of results and paginate it

### Setup ###

Input credentials to Web.config, AwsAccessKey, AwsSecretKey, AwsAssociateId and OexAppId (openexchangerates.org App Id) are required.